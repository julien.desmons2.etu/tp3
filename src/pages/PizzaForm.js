import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
        const form = document.querySelector('form');
    
        form.addEventListener('submit', function(event) {
            event.preventDefault();
            PizzaForm.submit(event);
        });
    
	}

	static submit(event) {
        const form = document.querySelector('form'),
	    input = form.querySelector('input[name=name]');

        if(input.value) {
            alert(`La pizza ${input.value} a été ajouté !`);
            input.value = '';
        } else {
            alert('Champ vide !');
        }

    }
}
