import PizzaThumbnail from '../components/PizzaThumbnail.js';
import Page from './Page';

export default class PizzaList extends Page {
	#pizzas;

	constructor(pizzas) {
		super('pizzaList');
		this.pizzas = pizzas;
	}

	set pizzas(value) {
		this.#pizzas = value;
		this.children = this.#pizzas.map(pizza => new PizzaThumbnail(pizza));
	}
}
