export default class Router {	
	static titleElement;
	static contentElement;
	static routes = [];

	static #menuElement;

	static set menuElement(element) {
		this.#menuElement = element;
		document.querySelectorAll('.mainMenu a').forEach(element => {
			element.addEventListener('click', event => {
				event.preventDefault();
				window.history.pushState(null,null,element.getAttribute('href'));
				this.navigate(element.getAttribute('href'));
			})
		})
		// au clic sur n'importe quel lien contenu dans "element"
		// déclenchez un appel à Router.navigate(path)
		// où "path" est la valeur de l'attribut `href=".."` du lien cliqué
	}

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			document.querySelector('.active')?.removeAttribute('class');
			document.querySelectorAll('.mainMenu a').forEach(element => {
				if(element.getAttribute('href') === path) {
					element.setAttribute('class', 'active');
				}
			})
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
		}
	}
}
