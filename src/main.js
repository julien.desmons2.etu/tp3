import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

//const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];
pizzaList.pizzas = data;

document.querySelector('.newsContainer').setAttribute('style','');

document.querySelector('.closeButton').addEventListener('click', event => {
    event.preventDefault();
    document.querySelector('.newsContainer').setAttribute('style','display:none');
})


Router.routes = [
    { path: '/', page: pizzaList, title: 'La carte' },
    { path: '/a-propos', page: aboutPage, title: 'À propos' },
    { path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');

Router.navigate(document.location.pathname);

window.onpopstate = function() {
    let temp = document.location.pathname;
    Router.navigate(temp);
}
    